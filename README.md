# Press F to pay respect!
An assembler programm, which emulates a printer in MCU 8051 IDE.

just load the BCD-configuration into an 1xANY (1x36 recommended) and start the program.

Press any external interrupt to change the error exception.

special thanks to nogrods repo for switching any supported text, to fit into the hex code of the BCD-display.
https://gitlab.com/Nogrod/intohex